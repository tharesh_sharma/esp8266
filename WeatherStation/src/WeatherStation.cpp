
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include "JsonStreamingParser.h"
#include "JsonListener.h"
#include "WundergroundClient.h"
#include "WundergroundHourly.h"
#include <ArduinoJson.h>

/**
 * Wunderground Settings
 */
const String WUNDERGRROUND_API_KEY = "9ff9fc91b0174af2b9fc91b0177af254";
const String WUNDERGR_UND_STATE_OR_COUNTRY = "CH";
const String WUNDERGR_UND_CITY = "ZURICH";
const String WUNDERGRROUND_LANGUAGE = "EN"; // as per https://www.wunderground.com/weather/api/d/docs?d=resources/country-to-iso-matching
const boolean IS_METRIC = true;
const boolean IS_24HOURS = false;
WundergroundHourly wunderground(IS_METRIC, IS_24HOURS);
const char *ssid = "SwatiTharesh";
const char *password = "h52529NqDs8b27Awok0Es0O7bhefUkFO";
const String API_KEY = "9ff9fc91b0174af2b9fc91b0177af254";
const String STATION_ID = "IBADENWR336";
const String UNITS = "m";
const String FORMAT = "json";
const String url = "http://api.weather.com/v2/pws/observations/current?stationId=" + STATION_ID + "&format=" + FORMAT + "&units=" + UNITS + "&apiKey=" + API_KEY;
const char *host = "https://192.168.0.92/";
const size_t capacity = JSON_ARRAY_SIZE(1) + JSON_OBJECT_SIZE(1) + JSON_OBJECT_SIZE(10) + JSON_OBJECT_SIZE(16) + 370;
DynamicJsonDocument doc(capacity);

void setup()
{
  // initialize LED digital pin as an output.
  Serial.begin(115200);
  delay(100);
  Serial.println();
  Serial.printf("Connecting to %s", ssid);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    Serial.print(".");
  }

  Serial.println();
  Serial.printf("Connected to %s Wifi\n", ssid);
  Serial.printf("IP Address : %s\n", WiFi.localIP().toString().c_str());

  Serial.println();
  Serial.println("\n\nNext Loop-Step: " + String(millis()) + ":");
}

void loop()
{
  if (WiFi.status() == WL_CONNECTED)
  {
    Serial.println("Getting weather data from : ");
    Serial.println(url);

    WiFiClient wifi_client;
    wifi_client.connect(host, 80);

    HTTPClient http_client;
    http_client.begin(wifi_client, url);
    int http_code = http_client.GET();

    if (http_code > 0)
    {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] GET... code : %d\n", http_code); // file found at server

      if (http_code == HTTP_CODE_OK)
      {
        // Deserialize the JSON document
        String payload = http_client.getString();
        DeserializationError error = deserializeJson(doc, payload);

        // Test if parsing succeeds.
        if (error)
        {
          Serial.println("parseObject() failed");
          return;
        }

        JsonObject observations_0 = doc["observations"][0];
        const char *observations_0_stationID = observations_0["stationID"];       // "IBADENWR336"
        const char *observations_0_obsTimeUtc = observations_0["obsTimeUtc"];     // "2020-06-25T08:32:00Z"
        const char *observations_0_obsTimeLocal = observations_0["obsTimeLocal"]; // "2020-06-25 10:32:00"
        const char *observations_0_neighborhood = observations_0["neighborhood"]; // "Scharnhausen"
        const char *observations_0_softwareType = observations_0["softwareType"]; // "weewx-3.9.2"
        const char *observations_0_country = observations_0["country"];           // "DE"
        float observations_0_lon = observations_0["lon"];                         // 9.262343
        long observations_0_epoch = observations_0["epoch"];                      // 1593073920
        float observations_0_lat = observations_0["lat"];                         // 48.707489
        int observations_0_winddir = observations_0["winddir"];                   // 47
        int observations_0_humidity = observations_0["humidity"];                 // 56
        int observations_0_qcStatus = observations_0["qcStatus"];                 // 1

        JsonObject observations_0_metric = observations_0["metric"];
        int observations_0_metric_temp = observations_0_metric["temp"];               // 22
        int observations_0_metric_heatIndex = observations_0_metric["heatIndex"];     // 22
        int observations_0_metric_dewpt = observations_0_metric["dewpt"];             // 13
        int observations_0_metric_windChill = observations_0_metric["windChill"];     // 22
        int observations_0_metric_windSpeed = observations_0_metric["windSpeed"];     // 3
        int observations_0_metric_windGust = observations_0_metric["windGust"];       // 10
        float observations_0_metric_pressure = observations_0_metric["pressure"];     // 1017.81
        int observations_0_metric_precipRate = observations_0_metric["precipRate"];   // 0
        int observations_0_metric_precipTotal = observations_0_metric["precipTotal"]; // 0
        int observations_0_metric_elev = observations_0_metric["elev"];               // 311
        // Print values.
        Serial.printf("Station ID       : %s\n", observations_0_stationID);
        Serial.printf("Time UTC         : %s\n", observations_0_obsTimeUtc);
        Serial.printf("Time Local       : %s\n", observations_0_obsTimeLocal);
        Serial.printf("Neighborhood     : %s\n", observations_0_neighborhood);
        Serial.printf("Country          : %s\n", observations_0_country);
        Serial.printf("Software Type    : %s\n", observations_0_softwareType);
        Serial.printf("Longitude        : %.4f\n", observations_0_lon);
        Serial.printf("Latitude         : %.4f\n", observations_0_lat);
        Serial.printf("Epoch            : %ld\n", observations_0_epoch);
        Serial.printf("Wind Direction   : %d\n", observations_0_winddir);
        Serial.printf("Humidity         : %d\n", observations_0_humidity);
        Serial.printf("QC Status        : %d\n", observations_0_qcStatus);
        Serial.printf("Temp             : %d\n", observations_0_metric_temp);
        Serial.printf("Heat Index       : %d\n", observations_0_metric_heatIndex);
        Serial.printf("Dew Point        : %d\n", observations_0_metric_dewpt);
        Serial.printf("Wind Chill       : %d\n", observations_0_metric_windChill);
        Serial.printf("Wind Speed       : %d\n", observations_0_metric_windSpeed);
        Serial.printf("Wind Gust        : %d\n", observations_0_metric_windGust);
        Serial.printf("Pressure         : %.4f\n", observations_0_metric_pressure);
        Serial.printf("Precipitate Rate : %d\n", observations_0_metric_precipRate);
        Serial.printf("Precipit. Total  : %d\n", observations_0_metric_precipTotal);
        Serial.printf("Elevation        : %d\n", observations_0_metric_elev);
      }
    }
    else
    {
      Serial.printf("[HTTP] GET... failed, error: %s\n", http_client.errorToString(http_code).c_str());
    }
  }
  delay(60000);
}
/**
 * LOOP
 */
